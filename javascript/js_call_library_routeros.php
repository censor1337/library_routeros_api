<?php
header("Content-Type: application/json");
require((realpath(dirname(__FILE__, 2) . '/library_routeros.php')));

$api_lib = new routeros_api_library();
$API_Config = new RouterosAPI_Config();
/**
 * Set up config
 */
$auth = $API_Config->auth;

/**
 * this is for security check you need to setup this your self if you really need to use this.
 */

if (!isset($_POST['action'])) {
    print json_encode("Error no action");
    return;
}

if ($auth and (!(isset($_POST['auth_token'])))) {
    if (($_POST['auth_token']) == null) {
        print json_encode("Error no token");
        return;
    }
    print json_encode("Error token error");
    return;
}

if ($auth) {
    // Your auth function here
    print json_encode("Authentication needed");
    return;
}

if (method_exists($api_lib, $_POST['action'])) {

    $action = $_POST['action'];
    $parameter = ($_POST['parameter'] != null) ? $_POST['parameter'] : null;

    if (is_array($parameter)) {
        $getData = call_user_func_array(array($api_lib, $action), $parameter);
    } else {
        $getData = $api_lib->$action($parameter);
    }
    print json_encode($getData);
    return "wtf";
}
