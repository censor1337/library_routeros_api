function call_library_routeros(action = null, parameter = null) {
    AjaxOBJ = $.ajax({
        type: "POST",
        url: "./js_call_library_routeros.php",
        error: function (ts) {
            //console.log(ts.responseText);
        }, // or console.log(ts.responseText)
        dataType: "json",
        data: {
            action: action,
            parameter: parameter
        }
    });

    AjaxOBJ.done(function (ResponsedJson) {
        alert("Done");
        return ResponsedJson;

    });

    AjaxOBJ.fail(function (ResponsedJson) {
        console.log(ResponsedJson);
        //console.log(ResponsedJson);
        return "failed";
    });
}

async function call_library_routeros_auth(auth_token = null, action = null, parameter = null) {
    AjaxOBJ = $.ajax({
        type: "POST",
        url: "./lib/js_call_library_routeros.php",
        error: function (ts) {
            console.log(ts.responseText);
        }, // or console.log(ts.responseText)
        dataType: "json",
        data: {
            auth_token: auth_token,
            action: action,
            parameter: parameter
        }
    });

    AjaxOBJ.done(function (ResponsedJson) {
        console.log(ResponsedJson);
    });
}